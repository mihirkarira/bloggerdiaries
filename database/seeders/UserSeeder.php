<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Mihir Karira',
            'email'=> 'mihirkarira@gmail.com',
            'password'=> Hash::make('abcd123')
        ]);

        User::create([
            'name' => 'Mike Joe',
            'email'=> 'mike@joe.com',
            'password'=> Hash::make('abcd123')
        ]);

        User::create([
            'name' => 'Mihir',
            'email'=> 'mihirkarira8310@gmail.com',
            'password'=> Hash::make('abcd123')
        ]);



    }
}
