<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoryNews = Category::create(['name' => 'News']);
        $categoryDesign = Category::create(['name' => 'Design']);
        $categoryTechnology = Category::create(['name' => 'Technology']);
        $categoryEngineering = Category::create(['name' => 'Engineering']);

        $tagCustomers = Tag::create(['name' => 'Customers']);
        $tagDesign = Tag::create(['name' => 'Design']);
        $tagLaravel = Tag::create(['name' => 'Laravel']);
        $tagCoding = Tag::create(['name' => 'Coding']);

        $post1 = Post::create([
            'title' => 'We relocated our office to HOME!',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'images/posts/1.jpg',
            'category_id' => $categoryDesign->id,
            'user_id' => 1,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post2 = Post::create([
            'title' => 'Corona vaccination drive is on',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'images/posts/2.jpg',
            'category_id' => $categoryNews->id,
            'user_id' => 1,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post3 = Post::create([
            'title' => "Apple's iwatch series 7 is coming soon !!",
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'images/posts/3.jpg',
            'category_id' => $categoryTechnology->id,
            'user_id' => 2,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post4 = Post::create([
            'title' => "All new windows 11 is here !!",
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'images/posts/4.jpg',
            'category_id' => $categoryDesign->id,
            'user_id' => 1,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post5 = Post::create([
            'title' => "Laravel is awesome",
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'images/posts/5.jpg',
            'category_id' => $categoryEngineering->id,
            'user_id' => 2,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        // Creating Entries for bridging table: post_tag
        $post1->tags()->attach([$tagCustomers->id, $tagDesign->id]);
        $post2->tags()->attach([$tagCustomers->id]);
        $post3->tags()->attach([$tagDesign->id]);
        $post4->tags()->attach([$tagCoding->id]);
        $post5->tags()->attach([$tagCoding->id, $tagDesign->id, $tagLaravel->id]);

    }
}
