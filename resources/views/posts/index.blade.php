@extends('layouts.admin-panel.app')

@section('page-level-styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.min.css">
@endsection

@section('content')
    <div class="d-flex justify-content-end mb-3">
        <a href="{{ route('posts.create')}}" class= "btn btn-outline-primary" >Add Post</a>
    </div>
    <div class="card">
        <div class="card-header">
            <h2>New Post</h2>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                  <tr>
                    <th scope="col">Image</th>
                    <th scope = "col">Title</th>
                    <th scope="col">Excerpt</th>
                    <th scope="col">Category</th>
                    <th scope = "col">Action</th>
                    <th scope = "col">Post Controls</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($posts as $post)
                        <tr>
                            <td><img src="{{ asset($post->image_path)}}" width="120"></td>
                            <td>{{$post->title}}</td>
                            <td>{{$post->excerpt}}</td>
                            <td>{{$post->category->name}}</td>
                            <td>
                                <a href="{{route('posts.edit', $post->id)}}" class="btn btn-sm btn-primary">Edit</a>
                                <button type="button" class="btn btn-sm btn-danger" onclick="displayModal({{$post->id }})" data-toggle="modal"
                                    data-target="#deleteModal">Trash</button>
                            </td>
                            @if(auth()->user()->isAdmin())
                            <td>
                                @if (!$post->isApproved())
                                    <form action="{{route('posts.approve', $post->id)}}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <button type="submit" class="btn btn-sm btn-outline-success">
                                            <i class="fas fa-check"></i>
                                            Approve Post
                                        </button>
                                    </form>
                                @else
                                    <form action="{{route('posts.disapprove', $post->id)}}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <button type="submit" class="btn btn-sm btn-outline-danger">
                                            <i class="fas fa-times"></i>
                                            Disapprove Post
                                        </button>
                                    </form>
                                @endif
                            <td>
                        @endif
                        </tr>
                    @endforeach
                </tbody>
              </table>
        </div>
        <!-- Delete Modal -->
        <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="" method="POST" id="deletePostForm">
                        @csrf
                        @method('DELETE')
                        <div class = "modal-body">
                            Are You Sure, You wanna delete modal ??
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-outline-danger">Delete</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-5">
        {{$posts->links('vendor.pagination.bootstrap-4')}}
    </div>
@endsection

@section('page-level-scripts')
      <script>
          function displayModal(postId) {
              var url = "/posts/trash/" + postId;
              $("#deletePostForm").attr('action', url);
            }
      </script>
@endsection
