@extends('layouts.admin-panel.app')

@section('title', 'Blogger Diaries')

@section('content')
    <div class="card">
        <div class="card-header h2">Users</div>
        <div class="card-body">
            <table class="table">

                    <tr>
                        <th scope="col">Image</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Post</th>
                        {{-- <th scope="col">Content</th> --}}
                        <th scope="col">Actions</th>
                    </tr>
                <tbody>
                    @foreach ($users as $user)
                    <tr>
                        <th scope="row">
							<img src="{{$user->gravatar_image}}" width="80">
						</td>
                        <th scope="row">{{ $user->name }}</td>
                        <th scope="row">{{ $user->email }}</td>
                        <th scope="row">{{ $user->posts->count()}}</td>
						<td>
                            @if(! $user->isAdmin())
                                <form action="{{ route('users.make-admin', $user->id)}}" method = "POST">
                                    @csrf
                                    @method('PUT')
                                    <button type = "submit" class="btn btn-sm btn-outline-success">Make Admin</button>
                                </form>
                            @else
                            <form action="{{ route('users.revoke-admin', $user->id)}}" method = "POST">
                                @csrf
                                @method('PUT')
                                <button type = "submit" class="btn btn-sm btn-outline-success">Revoke Admin</button>
                            </form>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
        </div>
    </div>
    <div class="mt-5">
        {{ $users->links('vendor.pagination.bootstrap-4') }}
    </div>

    <!-- DELETE MODAL -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you want to delete this post?</p>
            </div>
            <div class="modal-footer">
                <form action="" method="POST" id="deletepostForm">
                    @csrf
                    @method('DELETE')
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Delete post">
                </form>
            </div>
            </div>
        </div>
    </div>

@endsection

@section('page-level-scripts')
    <script>
        function displayModal(postId) {
            var url = '/posts/trash/' + postId;
            $('#deletepostForm').attr('action', url);
        }
    </script>
@endsection





